#include <iostream>
#include <vector>

#define MAX_CHARACTERS 256

using namespace std;

typedef struct trie_data_structure
{
    char                       character;                    //Holds character of the string.
    int                        count;                        //Contains count of the word, if the character is end of the word.
    bool                       isEndOfWord;                  //set to true if the character is the last character of the word.
    int                        totalNumberOfChildren;        //Contains total number of children nodes.
    struct trie_data_structure *children[MAX_CHARACTERS];    //holds the list of children with same suffixes.
}trie_data_struct;

class TrieDataStructureClass
{
    private:
/******************************************************************************************************************************************************************************************************************************
 Name:        createNewNode
 Parameters:  character (char)
 Return Type: trie_data_struct (trie data structure type)
 Description: Creates new trie data structure node with specific character.
*******************************************************************************************************************************************************************************************************************************/
        trie_data_struct* createNewNode(char character)
        {
            trie_data_struct *newTrieNode = new trie_data_struct();
            if(newTrieNode)
            {
                newTrieNode->character             = character;
                newTrieNode->isEndOfWord           = false;
                newTrieNode->totalNumberOfChildren = 0;
                *(newTrieNode->children)           = NULL;
            }
            return newTrieNode;
        }
    
    
    
/******************************************************************************************************************************************************************************************************************************
 Name:        checkAndReturnChildNodeReferenceOfANodeWithTheCharacterStartingFromNode
 Parameters:  char (character) , trie_data_struct (trie data structure type)
 Return Type: trie_data_struct (trie data structure type)
 Description: Checks and returns the trie node if the child node of specific trie node with specified character exists otherwise returns NULL.
 *******************************************************************************************************************************************************************************************************************************/
        trie_data_struct* checkAndReturnChildNodeReferenceOfANodeWithTheCharacterStartingFromNode(char character , trie_data_struct *trieNode)
        {
            for(int i = 0 ; i < trieNode->totalNumberOfChildren ; i++)
            {
                trie_data_struct *trieNodesChildNode = trieNode->children[i];
                if(trieNodesChildNode->character == character)
                {
                    return trieNodesChildNode;
                }
            }
            return NULL;
        }
    
    
    
/******************************************************************************************************************************************************************************************************************************
 Name:        createTrieDataStructureFromStringsVectorAtRoot
 Parameters:  stringsVector (vector)
 Return Type: void
 Description: Creates complete trie data structure of giver vector of strings.
 *******************************************************************************************************************************************************************************************************************************/
        trie_data_struct* createTrieDataStructureFromStringsVectorAtRoot(vector<string> stringsVector)
        {
            trie_data_struct *rootNode = createNewNode('\0');
            int stringsVectorSize      = (int)stringsVector.size();
            for(int i = 0 ; i < stringsVectorSize ; i++)
            {
                trie_data_struct *nodeForInsertion            = rootNode;
                string           currentVectorIterationString = stringsVector[i];
                int              currStringCurrentIndex       = -1;
                while (currentVectorIterationString[++currStringCurrentIndex] != '\0')
                {
                    trie_data_struct *childReferenceNode = checkAndReturnChildNodeReferenceOfANodeWithTheCharacterStartingFromNode(currentVectorIterationString[currStringCurrentIndex] , nodeForInsertion);
                    if(!childReferenceNode)
                    {
                        nodeForInsertion->children[nodeForInsertion->totalNumberOfChildren]  = createNewNode(currentVectorIterationString[currStringCurrentIndex]);
                        nodeForInsertion->totalNumberOfChildren                             += 1;
                        nodeForInsertion                                                     = nodeForInsertion->children[nodeForInsertion->totalNumberOfChildren - 1];
                    }
                    else
                    {
                        nodeForInsertion = childReferenceNode;
                    }
                }
                nodeForInsertion->isEndOfWord  = true;
                nodeForInsertion->count       += 1;
            }
            return rootNode;
        }
    
    
    
/******************************************************************************************************************************************************************************************************************************
 Name:        getMaximumMatchedCharacterOfStringInTrieStructure
 Parameters:  toBeCheckedString (string) , trieRootNode (Trie Data Structure type)
 Return Type: trie_data_struct (Trie Data Structure type)
 Description: Returns the maximum node matched if traversal reached till last character of the toBeCheckedString in the trie structure, otherwise returns Null.
              This method is made to avoid repetition of code in checkIfTheStringExistsInTrie and findNumberOfOccurencesOfStringInTrie methods.
 *******************************************************************************************************************************************************************************************************************************/
        trie_data_struct* getMaximumMatchedCharacterOfStringInTrieStructure(string toBeCheckedString , trie_data_struct *trieRootNode)
        {
            int currentIndex = -1;
            while (toBeCheckedString[++currentIndex] != '\0')
            {
                trieRootNode = checkAndReturnChildNodeReferenceOfANodeWithTheCharacterStartingFromNode(toBeCheckedString[currentIndex] , trieRootNode);
                if(!trieRootNode)
                {
                    break;
                }
            }
            return trieRootNode;
        }
    
    
    
/******************************************************************************************************************************************************************************************************************************
 Name:        parseTrieStructureTillPrefixNodeAndGetTheNode
 Parameters:  trieNode (trie_data_struct type) , prefixString (string)
 Return Type: trie_data_struct (trie data structure type)
 Description: Parse the trie structure till specified prefix character and return the node if available, otherwise return NULL.
 *******************************************************************************************************************************************************************************************************************************/
        trie_data_struct* parseTrieStructureTillPrefixNodeAndGetTheNode(trie_data_struct *trieRootNode , string prefixString)
        {
            int currentIndex = -1;
            while (prefixString[++currentIndex] != '\0')
            {
                for(int i = 0 ; i < trieRootNode->totalNumberOfChildren ; i++)
                {
                    if(trieRootNode->children[i]->character == prefixString[currentIndex])
                    {
                        trieRootNode = trieRootNode->children[i];
                        break;
                    }
                }
                if (trieRootNode->character != prefixString[currentIndex])
                {
                    return NULL;
                }
            }
            return trieRootNode;
        }
    
    
    
/******************************************************************************************************************************************************************************************************************************
 Name:        findAllPossibleWordsStartingFromNodeInTrieRecursively
 Parameters:  trieNode (trie_data_struct type) , prefixString (string) , suffixString (string) , allPossibleStringsVector (vector<string>)
 Return Type: vector<string> (vector of strings)
 Description: Returns vector containing all possible strings starting with specified prefix
 *******************************************************************************************************************************************************************************************************************************/
        vector<string> findAllPossibleWordsStartingFromNodeInTrieRecursively(trie_data_struct *trieNode , string prefixString , string suffixString , vector<string> allPossibleStringsVector)
        {
            if(!(trieNode->totalNumberOfChildren))
            {
                allPossibleStringsVector.push_back(prefixString + suffixString);
                return allPossibleStringsVector;
            }
            for(int i = 0 ; i < trieNode->totalNumberOfChildren ; i++)
            {
                string modifiedSuffixString = suffixString + trieNode->children[i]->character;
                allPossibleStringsVector    = findAllPossibleWordsStartingFromNodeInTrieRecursively(trieNode->children[i] , prefixString , modifiedSuffixString , allPossibleStringsVector);
            }
            return allPossibleStringsVector;
        }
    
    

    public:
/******************************************************************************************************************************************************************************************************************************
 Name:        getTrieFromVector (Public API)
 Parameters:  stringsVector (vector)
 Return Type: trie_data_struct (Trie Data Structure type)
 Description: Public API for creating Trie from input vector
 *******************************************************************************************************************************************************************************************************************************/
        trie_data_struct* getTrieFromVector(vector<string> stringsVector)
        {
            return createTrieDataStructureFromStringsVectorAtRoot(stringsVector);
        }
    
    
    
/******************************************************************************************************************************************************************************************************************************
 Name:        checkIfTheStringExistsInTrie (Public API)
 Parameters:  toBeCheckedString (string) , trieRootNode (Trie Data Structure type)
 Return Type: bool
 Description: Returns true if the given string is present in trie, otherwise false.
 *******************************************************************************************************************************************************************************************************************************/
        bool checkIfTheStringExistsInTrie(string toBeCheckedString , trie_data_struct *trieRootNode)
        {
            trieRootNode = getMaximumMatchedCharacterOfStringInTrieStructure(toBeCheckedString , trieRootNode);
            return (trieRootNode && trieRootNode->isEndOfWord ? true : false);
        }
    
    
    
/******************************************************************************************************************************************************************************************************************************
 Name:        findNumberOfOccurencesOfStringInTrie (Public API)
 Parameters:  toBeCheckedString (string) , trieRootNode (Trie Data Structure type)
 Return Type: int
 Description: Returns the count of occurences of toBeCheckedString in the trie structure.
 *******************************************************************************************************************************************************************************************************************************/
        int findNumberOfOccurencesOfStringInTrie(string toBeCheckedString , trie_data_struct *trieRootNode)
        {
            trieRootNode = getMaximumMatchedCharacterOfStringInTrieStructure(toBeCheckedString , trieRootNode);
            return (trieRootNode && trieRootNode->isEndOfWord ? trieRootNode->count : 0);
        }
    
    
/******************************************************************************************************************************************************************************************************************************
 Name:        getTheListVectorOfAllPossibleWordsInTrieStructureWithPrefix (Public API)
 Parameters:  trieRootNode (Trie Data Structure type) , prefixString (string)
 Return Type: vector<string>  (Vector containing strings).
 Description: Returns the vector of all possible strings with prefix prefixString.
 *******************************************************************************************************************************************************************************************************************************/
        vector<string> getTheListVectorOfAllPossibleWordsInTrieStructureWithPrefix(trie_data_struct *trieRootNode , string prefixString)
        {
            vector<string> allPossibleStringsVector;
            trieRootNode = parseTrieStructureTillPrefixNodeAndGetTheNode(trieRootNode , prefixString);
            allPossibleStringsVector = findAllPossibleWordsStartingFromNodeInTrieRecursively(trieRootNode , prefixString , "" , allPossibleStringsVector);
            return allPossibleStringsVector;
        }
    
    
/******************************************************************************************************************************************************************************************************************************
 Name:        getTheListVectorOfAllPossibleWordsInTrieStructureWithPrefix (Public API)
 Parameters:  trieRootNode (Trie Data Structure type) , prefixString (string)
 Return Type: vector<string>  (Vector containing strings).
 Description: Returns the vector of all possible strings with prefix prefixString.
 *******************************************************************************************************************************************************************************************************************************/
        void printAllStringsInsideVector(vector<string> allWordsVector , string prefixString)
        {
            int allWordsVectorSize = (int)allWordsVector.size();
            if (!allWordsVectorSize)
            {
                cout<<"No string starting with "<<prefixString<<" is available in the trie structure.";
                return;
            }
            cout<<"List of all available strings starting with "<<"'"<<prefixString<<"'"<<" in the trie structure : "<<endl;
            for(int i = 0 ; i < allWordsVectorSize ; i++)
            {
                cout<<allWordsVector[i]<<endl;
            }
        }
};

//Driver Method
int main(int argc, const char * argv[])
{
    vector<string>         allWordsVector                  = {"the" , "there" , "these" , "them" , "thsdfsf" , "thesdf" , "the" , "past" , "future"};
    string                 toBeFoundString                 = "past";
    string                 prefixStrForAllAvailableString  = "these";
    TrieDataStructureClass trieDataStructureObject = TrieDataStructureClass();
    trie_data_struct       *trieRootNode           = trieDataStructureObject.getTrieFromVector(allWordsVector);
    cout<<"Word "<<"'"<<toBeFoundString<<"'"<<(trieDataStructureObject.checkIfTheStringExistsInTrie(toBeFoundString , trieRootNode) ? " is present in the structure." : " is not present in the structure.")<<endl;
    cout<<"Word "<<"'"<<toBeFoundString<<"'"<<" occurs "<<trieDataStructureObject.findNumberOfOccurencesOfStringInTrie(toBeFoundString , trieRootNode)<<" times in the structure."<<endl;
    
//    vector<string> allPossibleStringsVector        = trieDataStructureObject.getTheListVectorOfAllPossibleWordsInTrieStructureWithPrefix(trieRootNode , "the");
    trieDataStructureObject.printAllStringsInsideVector(trieDataStructureObject.getTheListVectorOfAllPossibleWordsInTrieStructureWithPrefix(trieRootNode , prefixStrForAllAvailableString) , prefixStrForAllAvailableString);
    return 0;
}
